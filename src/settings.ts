import * as settings from 'electron-settings';
import { app } from 'electron';
import * as path from 'path';

settings.configure({
	dir: path.join(app.getPath('userData'), 'settings'),
	fileName: 'global.json',
	prettify: true,
	numSpaces: 4
});

export function getSetting(key: string, def: any = null): any {
	const h = settings.hasSync(key);
	if (h) {
		return settings.getSync(key);
	} else {
		settings.setSync(key, def);
		return def;
	}
}
