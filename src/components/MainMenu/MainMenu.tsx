import * as React from 'react';
import { Navbar, NavbarGroup, NavbarHeading, NavbarDivider, Alignment, MenuItem, Button, Menu } from '@blueprintjs/core';

import './MainMenu.css';
import icon from '../../static/icon/24x24.png';
import { Classes, Popover2 } from '@blueprintjs/popover2';

export function MainMenu() {
	return (
		<div className='main-menu'>
			<Navbar>
				<NavbarGroup align={ Alignment.LEFT }>
					<img src={ icon } className='menu-icon'></img>
					<NavbarHeading>Git Compass</NavbarHeading>
					<NavbarDivider />
					<Popover2
						interactionKind="click"
						popoverClassName={ Classes.POPOVER2_CONTENT_SIZING }
						placement="bottom-start"
						inheritDarkTheme={ true }
						minimal={ true }
						renderTarget={ ({ isOpen, ref, ...targetProps }) => (
							<Button { ...targetProps } elementRef={ ref } text="File" className='menu-root-item' />
						) }
						content={
							<Menu className='custom-menu'>
								<MenuItem className='custom-menu-item' text='asdf' icon='airplane' />
							</Menu>
						}
					/>
					<Popover2
						interactionKind="click"
						popoverClassName={ Classes.POPOVER2_CONTENT_SIZING }
						placement="bottom-start"
						inheritDarkTheme={ true }
						minimal={ true }
						renderTarget={ ({ isOpen, ref, ...targetProps }) => (
							<Button { ...targetProps } elementRef={ ref } text="Git" className='menu-root-item' />
						) }
						content={
							<Menu className='custom-menu'>
								<MenuItem className='custom-menu-item' text='asdf' icon='airplane' />
							</Menu>
						}
					/>
					<Popover2
						interactionKind="click"
						popoverClassName={ Classes.POPOVER2_CONTENT_SIZING }
						placement="bottom-start"
						inheritDarkTheme={ true }
						minimal={ true }
						renderTarget={ ({ isOpen, ref, ...targetProps }) => (
							<Button { ...targetProps } elementRef={ ref } text="Help" className='menu-root-item' />
						) }
						content={
							<Menu className='custom-menu'>
								<MenuItem className='custom-menu-item' text='asdf' icon='airplane' />
							</Menu>
						}
					/>
					<Button icon='minimize' className='nav-bar-window-control nav-bar-window-split' />
					<Button icon='maximize' className='nav-bar-window-control' />
					<Button icon='cross' className='nav-bar-window-control close-btn' onClick={ () => {
						window.api.exit();
					} } />
				</NavbarGroup>
			</Navbar>
		</div>
	);
}