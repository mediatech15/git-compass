import * as React from 'react';
import * as ReactDOM from 'react-dom/client';
import { MainMenu } from './components/MainMenu/MainMenu';

const root = ReactDOM.createRoot(
	document.getElementById('root')
);

function MainLayout() {
	return (
		<div className='grid-container'>
			<MainMenu />
		</div>
	);
}

root.render(<MainLayout />);