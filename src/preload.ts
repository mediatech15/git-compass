import { contextBridge, ipcRenderer } from 'electron';

export type ContextBridgeApi = {
	exit: () => void;
};

const exposedApi: ContextBridgeApi = {
	exit: () => {
		ipcRenderer.send('app-close');
	},
};


contextBridge.exposeInMainWorld('api', exposedApi);